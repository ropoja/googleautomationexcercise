import org.testng.annotations.Test;
import pages.MainPage;

public class SearchWithGoogleTest extends BaseTest {

    protected SearchWithGoogleTest(String browserName) {
        super(browserName);
    }

    @Test
    public void search() {
        new MainPage(getDriver(), getDriverWait()).search("Hola Mundo");
    }

}
