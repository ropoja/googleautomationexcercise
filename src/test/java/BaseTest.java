import constants.Strings;
import lombok.Data;
import manage.DriverFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

public class BaseTest {
    private WebDriver driver;
    private WebDriverWait wait;

    @Parameters({"browserName"})
    protected BaseTest(String browserName) {
        this.driver = DriverFactory.createDriver(browserName);
        driver.get(Strings.GOOGLE_URL);
    }

    protected WebDriver getDriver() {
        return this.driver;
    }

    protected WebDriverWait getDriverWait() {
        return this.wait;
    }

}
