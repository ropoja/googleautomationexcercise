package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MainPage extends BasePage {
    @FindBy(name = "q")
    private WebElement txtSearch;

    @FindBy(name = "btnK")
    private WebElement btnSearch;

    @FindBy(name = "btnI")
    private WebElement btnLuckySearch;

    public MainPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    public void search(String textToSearch) {
        txtSearch.sendKeys(textToSearch);
        btnSearch.click();
    }
}
