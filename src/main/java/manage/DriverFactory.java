package manage;

import constants.Waits;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

public class DriverFactory {
    private DriverFactory() {

    }

    public static WebDriver createDriver(String browserName) {
        WebDriver webDriver;

        if ("firefox".equals(browserName)) {
            webDriver = new FirefoxDriver();
        } else {
            webDriver = new ChromeDriver();
        }

        webDriver.manage().timeouts().implicitlyWait(Waits.IMPLICIT, TimeUnit.SECONDS);
        return webDriver;
    }
}
